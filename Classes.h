#ifndef CLASSES_H
#define CLASSES_H
#include <algorithm>
const int NB_COORD=1000;

struct coord{
    int x;
    int y;
    };

struct ensCoord{
    coord tab[NB_COORD];
    int taille;
    };

class Fourmi
{
    private:


    public:

        coord c;
        bool sucre;
        bool Nid;
        // Constructor
        Fourmi();

        // This fuction Create's a Foumi in the coordinate c
        void creerFourmi( coord co );

        // This fuction return's the coordinates of the Fourmi f
        coord lireCoord();

        // This fuction
        bool chercheSucre();

        // Decharge the suger charge ( what ever the hell thet means)
        void dechargerSucre();

        // charge the suger charge ( what ever the hell thet means)
        void chargerSucre();
};

class ensFourmis
{
    public:


    int taille;
    Fourmi tab[1000];
    ensFourmis(ensCoord enC);
    void chargerEnsFourmis(ensCoord enC);
};


class Places
{
    public:

    coord c;
    bool sucre;
    bool Nid;
    bool fourmi;
    float PheroNid;
    float PheroSucre;


    //Places();

    void creerPlaceVide(coord co);

    float lirePheroNid();

    float lirePheroSucre();

    bool contientFourmi();

    bool contientSucre();

    bool contientNid();

    bool surUnePiste();

    void poserSucre();

    void poserNid();

    void poserFourmi(Fourmi &f);

    void poserPheroNid( float phero);

    void poserPheroSucre();

    void diminuerPheroSucre();

    bool vide();

};

class grille
{
    public:
    int t ;
    Places enP[20][20];



    void chargerGrilleVide();
    void chargerPlace( coord c,Places &p);
    void rangerPlace(Places p);
    float distanceNid(Places p);
    ensCoord getVoisin(coord p);
};

/** support fuctions **/

    void deplacerFourmi(Fourmi& f, Places& pF, Places& pI){
        // Free the start place by setting the fourmi bool in it to 0
        pI.fourmi = false;
        // Move the fourmi to the end place
        pF.poserFourmi(f) ;
    };

    bool plusProcheNid(grille & g, Places p1, Places p2 ){
        if(g.distanceNid(p1) < g.distanceNid(p2)){
            return true;
        }else{
            return false;
        }
    }

    void placerNid( grille& g, ensCoord ec ){
        for(int i ; i < ec.taille; i++ ){
            coord c = ec.tab[i];
            g.enP[c.x][c.y].poserNid();
        }
    }

    void placerSucre( grille& g, ensCoord ec ){
        for(int i ; i < ec.taille; i++ ){
            coord c = ec.tab[i];
            g.enP[c.x][c.y].poserSucre();
        }
    }

    void placerFourmis(grille& g, ensFourmis& ensf){

        for (int i; i< ensf.taille; i++ ){
            coord c = ensf.tab[i].lireCoord();
            g.enP[c.x][c.y].fourmi = true;
            }

        }

    void lineariserPheroNid(grille& g){
        grille gc;
        for (int i = 0 ; i < g.t ; i ++ ){
            gc = g;
            for (int i=0 ; i < 20 ; i ++){
                for (int j=0 ; j < 20 ; j ++){
                    if (g.enP[i][j].Nid) continue ;
                    float mPh = 0;
                    ensCoord enC;
                    enC = g.getVoisin(g.enP[i][j].c) ;
                    for ( int k = 0; k < enC.taille ; k++ ){
                        if(mPh < g.enP[enC.tab[k].x ][enC.tab[k].y].PheroNid ){
                            mPh = g.enP[enC.tab[k].x ][enC.tab[k].y].PheroNid;
                            }
                        }

                    gc.enP[i][j].PheroNid = std::max( mPh - (1.0/g.t) , 0.0 );

                    }
                }
            g = gc;
            }

        }

    void initialiserGrille(grille& g , ensFourmis enF, ensCoord sucre, ensCoord nid ){

        placerNid(g,nid);
        lineariserPheroNid(g);

        placerSucre(g,sucre);

        placerFourmis(g,enF);

        }

#endif // CLASSES_H
