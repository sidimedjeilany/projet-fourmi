#include "Classes.h"
#include <cmath>

/** Fourmi class **/

// The Fourmi class Constructor
Fourmi::Fourmi(){

}

void Fourmi::creerFourmi(coord co){
    // Set Fourmi cooardinates to c
    this->c = co;
    this->sucre = false;
}

bool Fourmi::chercheSucre(){
    return not this->sucre;
}

coord Fourmi::lireCoord(){
    return this->c;
}

void Fourmi::dechargerSucre(){
    this->sucre = false;
}

void Fourmi::chargerSucre(){

    this->sucre = true;
}

void ensFourmis::chargerEnsFourmis(ensCoord enC){

    this->taille = enC.taille;
    for(int i; i< this->taille;i++){
        this->tab[i].creerFourmi(enC.tab[i]);
        }
    }


/** Class Places **/
// This function creates an empty place
void Places::creerPlaceVide(coord co){
 this->c = co;
 this->fourmi = false;
 this->Nid = false;
 this->sucre = false;
 this->PheroNid = 0.0;
 this->PheroSucre = 0.0;
}
// This function returns a bool value to whether the place contains a "Fourmi or not"
bool Places::contientFourmi(){
    return this->fourmi;
}

bool Places::contientNid(){
    return this->Nid;
}

bool Places::contientSucre(){
    return this->sucre;
}

void Places::diminuerPheroSucre(){
    // TO DO
}

float Places::lirePheroNid(){
    return this->PheroNid;
    }
float Places::lirePheroSucre(){
    return this->PheroSucre;
    }

void Places::poserNid(){
    this->Nid = true;
    poserPheroNid(1.0);
    }

void Places::poserPheroNid(float phero){

    this->PheroNid = phero;
    }

void Places::poserPheroSucre(){

    this->PheroSucre = 255;
    }

void Places::poserSucre(){
    this->sucre = true;
    }

bool Places::surUnePiste(){
    if(this->PheroSucre > 0)
        return true;
    else
        return false;
    }

void Places::poserFourmi(Fourmi& f){
    this->fourmi = true;
    f.c = this->c;
}

bool Places::vide(){
    return ( this->fourmi == false && this->Nid == false && this->sucre == false );
}

void grille::chargerGrilleVide(){
    this->t = 20;
    for (int i=0 ; i < 20 ; i ++){
        for (int j=0 ; j < 20 ; j ++){
            // creat coord
            coord c;
            c.x = i;
            c.y = j;
            this->enP[i][j].creerPlaceVide(c);
            }
        }
}

void grille::chargerPlace(coord c, Places &p){
    int x = c.x;
    int y = c.y;
    p = this->enP[x][y];
}

void grille::rangerPlace(Places p){
    int x = p.c.x;
    int y = p.c.y;
    this->enP[x][y] = p;
}

float grille::distanceNid(Places p){
    coord PPNid;
    float quadD = 2000.0;
    for (int i=0 ; i < 20 ; i ++){
        for (int j=0 ; j < 20 ; j ++){
            if(this->enP[i][j].contientNid()){
                PPNid = this->enP[i][j].c;
                float d = std::pow( PPNid.x - p.c.x ,2.0) + pow(PPNid.y - p.c.y,2.0);
                if(d <= quadD){
                    quadD = d;
                    }
                }
            }
    }
    return sqrt(quadD);
}

ensCoord grille::getVoisin(coord p){

    int x;
    int y;
    ensCoord enC;
    enC.taille = 0;
    for(int i = -1 ; i <2; i++){
        for(int j = -1 ; i < 2 ; i++){
            if(i == 0 && j == 0) continue;
            x = p.x +i;
            y = p.y +j;
            if( 0 <=x && x < t && 0 <=y && y < t){
                enC.tab[enC.taille].x = x;
                enC.tab[enC.taille].y = y;
                enC.taille+= 1 ;
                }
            }
        }
    return enC;
    }

